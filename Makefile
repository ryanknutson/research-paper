# set the name of your TeX file here
PAPER=rk_research

# output directory (with trailing slash)
# OUTDIR=./build/
OUTDIR=/Volumes/ramdisk/
# PDF=$(OUTDIR)$(PAPER).pdf

all: pdf 

# compile and open pdf
opdf: pdf open
	echo "compile successful, opening"

# compile pdf
pdf:
	pdflatex --output-directory=$(OUTDIR) $(PAPER).tex && \
	biber --output-directory=$(OUTDIR) $(PAPER).bcf && \
	pdflatex --output-directory=$(OUTDIR) $(PAPER).tex && \
	echo "compile successful"

# open pdf
open:
	echo "opening pdf..."; \
	open $(OUTDIR)$(PAPER).pdf

# clean up
clean:
	cd $(OUTDIR) && \
	rm $(OUTDIR)$(PAPER).aux $(OUTDIR)$(PAPER).bbl $(OUTDIR)$(PAPER).bcf $(OUTDIR)$(PAPER).blg $(OUTDIR)$(PAPER).log $(OUTDIR)$(PAPER).out $(OUTDIR)$(PAPER).pdf $(OUTDIR)$(PAPER).run.xml ||:
