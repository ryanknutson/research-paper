#!/bin/bash

#╔══════════════════════════╗
#║  Starting LaTeX Compile  ║
#║        First Pass        ║
#╚══════════════════════════╝

#╔═══════════════════════════╗
#║       Running Biber       ║
#║  Generating Bibliography  ║
#╚═══════════════════════════╝

#╔══════════════════════════╗
#║  Starting LaTeX Compile  ║
#║       Second Pass        ║
#╚══════════════════════════╝

#╔══════════════════════════╗
#║    Compile Successful    ║
#╚══════════════════════════╝

case $1 in

  first)
    printf "\e[35m╔══════════════════════════╗\n";
    printf "\e[35m║\e[36m  Starting Latex Compile  \e[35m║\n";
    printf "\e[35m║\e[33m        First Pass        \e[35m║\n";
    printf "\e[35m╚══════════════════════════╝\n";
    ;;

  biber)    
    printf "\e[34m╔═══════════════════════════╗\n";
    printf "\e[34m║\e[35m       Running Biber       \e[34m║\n";
    printf "\e[34m║\e[33m  Generating Bibliography  \e[34m║\n";
    printf "\e[34m╚═══════════════════════════╝\n";
    ;;

  second)
    printf "\e[35m╔══════════════════════════╗\n";
    printf "\e[35m║\e[36m  Starting Latex Compile  \e[35m║\n";
    printf "\e[35m║\e[33m       Second Pass        \e[35m║\n";
    printf "\e[35m╚══════════════════════════╝\n";
    ;;  

  succ)
    printf "\e[32m╔══════════════════════════╗\n";
    printf "\e[32m║\e[36m  Compiled Successfully!  \e[32m║\n";
    printf "\e[32m╚══════════════════════════╝\n";
    ;;

  *)
    printf "\e[31munknown option\n";
    ;;
esac
