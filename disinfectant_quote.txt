So I asked Bill a question that probably some of you are thinking of if you're
totally into that world, which I find to be very interesting. So, supposing we
hit the body with a tremendous, whether its ultraviolet or just very powerful
light, and I think you said that hasn't been checked but you're gonna test it.
And then I said, supposing you brought the light inside the body, which you can
do, either through the skin or in some other way, and I think you said you're
gonna test that too. Sounds interesting. And then I see the disinfectant where
it knocks it out in a minute, one minute, and is there a way we can do
something like that by injection inside, or almost a cleaning? Because you see
it gets in the lungs, and it does a tremendous number on the lungs, so it'd be
interesting to check that. So you're going to have to use medical doctors, but
it sounds interesting to me, so we'll see. But the whole concept of the light,
the way it goes in in one minute, that's pretty powerful.
